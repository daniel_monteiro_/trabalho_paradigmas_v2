# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny

from django.utils.encoding import smart_str, smart_unicode
import re


def remove_between_quotation(trecho):

    # t = re.sub('""', '', trecho)
    t = str(trecho).replace()

    print (t)

    return t


def get_verificador_l_pages(request):
    try:
        context = {
            "titulo": "titulo"
        }

        return render(request, 'index.html')
    except Exception, e:
        return


def get_maior_probabilidade(java, c, python, prolog, assembly):

    lista = [java, c, python, prolog, assembly]

    maior = 0

    for i in lista:
        if i > maior:
            maior = i

    return maior


def get_segunda_maior_probabilidade(java, c, python, prolog, assembly):

    lista = [java, c, python, prolog, assembly]

    maior = get_maior_probabilidade(java, c, python, prolog, assembly)
    segundo_maior = 0

    for i in lista:
        if maior > i > segundo_maior:
            segundo_maior = i

    return segundo_maior


@api_view(["POST"])
@permission_classes([AllowAny])
def send_verify_language(request):
    try:
        trecho_codigo = request.data.get("trecho")

        percent_java = get_percent_caracteristicas_java(smart_str(trecho_codigo).strip())
        percent_c = get_percent_caracteristicas_c(smart_str(trecho_codigo).strip())
        percent_python = get_percent_caracteristicas_python(smart_str(trecho_codigo).strip())
        percent_prolog = get_percent_caracteristicas_prolog(smart_str(trecho_codigo).strip())
        percent_assembly = get_percent_caracteristicas_assembly(smart_str(trecho_codigo).strip())

        dictLanguage = {
            percent_java: "Java",
            percent_c: "C",
            percent_python: "Python",
            percent_prolog: "Prolog",
            percent_assembly: "Assembly"
        }

        mais_provavel = get_maior_probabilidade(percent_java, percent_c, percent_python, percent_prolog, percent_assembly)
        segundo_mais_provavel = get_segunda_maior_probabilidade(percent_java, percent_c, percent_python, percent_prolog, percent_assembly)

        linguagem_mais_provavel = dictLanguage.__getitem__(mais_provavel)
        segunda_linguagem_mais_provavel = dictLanguage.__getitem__(segundo_mais_provavel)

        if mais_provavel < 9.0:
            contextReturn = {
                "valido": False,
                "linguagemMaisDetectada": "Não reconhecido. Pode não ser linguagem de programação.",
                "trecho": trecho_codigo
            }
            return render(request, 'index.html', contextReturn)

        contextReturn = {
            "valido": True,
            "linguagemMaisDetectada": linguagem_mais_provavel,
            "informacoesDeResultadoMaisDetectada": mais_provavel,
            "segundaLinguagemMaisDetectada": segunda_linguagem_mais_provavel,
            "informacoesDeResultadoSegundaMaisDetectada": segundo_mais_provavel,
            "trecho": trecho_codigo
        }

        return render(request, 'index.html', contextReturn)
    except Exception, e:
        return


def get_percent_caracteristicas_java(trecho):
    try:
        count = 0

        if smart_str(trecho).__contains__("{") or smart_str(trecho).__contains__("}"):
            count += 1

        if smart_str(trecho).__contains__(";") or smart_str(trecho).__contains__("(") or smart_str(trecho).__contains__(")"):
            count += 1

        if smart_str(trecho).__contains__("class ") or smart_str(trecho).__contains__("abstract ") or smart_str(trecho).__contains__(" extends "):
            count += 1

        if smart_str(trecho).__contains__("double ") or smart_str(trecho).__contains__("Double") or smart_str(trecho).__contains__("float ")\
                or smart_str(trecho).__contains__("int ") or smart_str(trecho).__contains__("String")\
                or smart_str(trecho).__contains__("char ") or smart_str(trecho).__contains__("Float"):
            count += 1

        if smart_str(trecho).__contains__("private") or smart_str(trecho).__contains__("public") or \
                smart_str(trecho).__contains__("protected") or smart_str(trecho).__contains__("default"):
            count += 1

        if smart_str(trecho).__contains__("();"):
            count += 1

        if smart_str(trecho).__contains__("List") or smart_str(trecho).__contains__("ArrayList"):
            count += 1

        if smart_str(trecho).__contains__("return"):
            count += 1

        if smart_str(trecho).__contains__("import"):
            count += 1

        if smart_str(trecho).__contains__("<>") or smart_str(trecho).__contains__("[]"):
            count += 1

        if smart_str(trecho).__contains__("if ") or smart_str(trecho).__contains__("else") or smart_str(trecho).__contains__("while") or \
                smart_str(trecho).__contains__("for"):
            count += 1

        if smart_str(trecho).__contains__("System.out.println") or smart_str(trecho).__contains__("System.out.print"):
            count += 1

        return float((float(count) / 12) * 100)
    except Exception, e:
        return 0


def get_percent_caracteristicas_c(trecho):
    try:
        count = 0

        if smart_str(trecho).__contains__("int main()"):
            count += 1

        if smart_str(trecho).__contains__("stdlib") or smart_str(trecho).__contains__("iostream") or smart_str(trecho).__contains__(
                "stdio"):
            count += 1

        if smart_str(trecho).__contains__("scanf") or smart_str(trecho).__contains__("printf("):
            count += 1

        if smart_str(trecho).__contains__("strcut") or smart_str(trecho).__contains__("typedef"):
            count += 1

        if smart_str(trecho).__contains__("define ") or smart_str(trecho).__contains__("max"):
            count += 1

        if smart_str(trecho).__contains__("usign ") or smart_str(trecho).__contains__("namespace") or smart_str(trecho).__contains__("void"):
            count += 1

        if smart_str(trecho).__contains__("template"):
            count += 1

        if smart_str(trecho).__contains__("{") or smart_str(trecho).__contains__("}"):
            count += 1

        if smart_str(trecho).__contains__(";") or smart_str(trecho).__contains__("(") or smart_str(trecho).__contains__(")"):
            count += 1

        if smart_str(trecho).__contains__("();"):
            count += 1

        if smart_str(trecho).__contains__("*") or smart_str(trecho).__contains__("**"):
            count += 1

        if smart_str(trecho).__contains__("<<") or smart_str(trecho).__contains__(">>"):
            count += 1

        if smart_str(trecho).__contains__("getch()"):
            count += 1

        if smart_str(trecho).__contains__("if ") or smart_str(trecho).__contains__("else") or smart_str(trecho).__contains__("while") or \
                smart_str(trecho).__contains__("for"):
            count += 1


        return float((float(count) / 14) * 100)
    except Exception, e:
        return 0


def get_percent_caracteristicas_python(trecho):
    try:
        count = 0

        if smart_str(trecho).__contains__("print") or smart_str(trecho).__contains__("print("):
            count += 1

        if smart_str(trecho).__contains__("if "):
            count += 1

        if smart_str(trecho).__contains__(" and ") or smart_str(trecho).__contains__(" or "):
            count += 1

        if smart_str(trecho).__contains__("else:") or smart_str(trecho).__contains__("elif "):
            count += 1

        if smart_str(trecho).__contains__("import ") or smart_str(trecho).__contains__("from "):
            count += 1

        if smart_str(trecho).__contains__("def ") or smart_str(trecho).__contains__("():") or smart_str(trecho).__contains__("):"):
            count += 1

        if smart_str(trecho).__contains__("try:") or smart_str(trecho).__contains__("Except"):
            count += 1

        if smart_str(trecho).__contains__("str(") or smart_str(trecho).__contains__("int(") or smart_str(trecho).__contains__("len("):
            count += 1

        if smart_str(trecho).__contains__("input") or smart_str(trecho).__contains__("input(") or smart_str(trecho).__contains__("return"):
            count += 1

        if not smart_str(trecho).__contains__("{") or not smart_str(trecho).__contains__("}"):
            count -= 1

        return float((float(count) / 10) * 100)
    except Exception, e:
        return 0


def get_percent_caracteristicas_prolog(trecho):
    try:
        count = 0

        if smart_str(trecho).__contains__(")."):
            count += 1

        if smart_str(trecho).__contains__(" , "):
            count += 1

        if smart_str(trecho).__contains__(":-"):
            count += 1

        if smart_str(trecho).__contains__("not("):
            count += 1

        if smart_str(trecho).__contains__("\\+") or smart_str(trecho).__contains__(">") or smart_str(trecho).__contains__("<=") or smart_str(trecho).__contains__(" \== "):
            count += 1

        if smart_str(trecho).__contains__(" is "):
            count += 1

        return float((float(count) / 6) * 100)
    except Exception, e:
        return 0


def get_percent_caracteristicas_assembly(trecho):
    try:
        count = 0

        if smart_str(trecho).upper().__contains__("AX,") or smart_str(trecho).upper().__contains__("BX,") or smart_str(trecho).upper().__contains__("AH,"):
            count += 1

        if smart_str(trecho).upper().__contains__("DX,") or smart_str(trecho).upper().__contains__("CX,") or smart_str(trecho).upper().__contains__("AL,"):
            count += 1

        if smart_str(trecho).upper().__contains__("MOV ") or smart_str(trecho).upper().__contains__("INT "):
            count += 1

        if smart_str(trecho).upper().__contains__("INC ") or smart_str(trecho).upper().__contains__("JMP ") or smart_str(trecho).upper().__contains__("SUB "):
            count += 1

        if smart_str(trecho).upper().__contains__("RET") or smart_str(trecho).upper().__contains__("LEA "):
            count += 1

        if smart_str(trecho).upper().__contains__("LOOP"):
            count += 1

        if smart_str(trecho).upper().__contains__("END"):
            count += 1

        return float((float(count) / 7) * 100)
    except Exception, e:
        return 0