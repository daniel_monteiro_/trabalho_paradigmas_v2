from __future__ import unicode_literals

from django.apps import AppConfig


class VerificadorLinguagensConfig(AppConfig):
    name = 'verificador_linguagens'
